#!/usr/bin/perl

use List::Util qw(reduce);

print("Part 1: " . &part1(@ARGV) . "\n");
print("Part 2: " . &part2(@ARGV) . "\n");

sub part1($) {
  my %pool = (
    "red"   => 12,
    "green" => 13,
    "blue"  => 14
  );

  open(F, $_[0]) or die "Couldn't open $_[0] for read: $!\n";
  my $result = 0;
  while (<F>) {
    chomp;
    my ($id, $game) = m/^Game (\d+): (.*?)$/;
    my @sets = split(/\s*;\s*/, $game);
    my $valid = 1;
    foreach (@sets) {
      my @cubes = split /\s*,\s*/;
      @cubes = map { [ m/(\d+)\s+(\w+)/ ] } @cubes;
      my @invalid = grep { $_->[0] > $pool{$_->[1]} } @cubes;
      if(@invalid) {
        $valid = 0;
        last;
      }
    }
    $result += $id if ($valid);
  }
  close(F);
  return $result;
}

sub part2($) {
  open(F, $_[0]) or die "Couldn't open $_[0] for read: $!\n";
  my $result = 0;
  while (<F>) {
    chomp;
    my ($id, $game) = m/^Game (\d+): (.*?)$/;
    my @sets = split(/\s*;\s*/, $game);
    my %colors = ();
    foreach (@sets) {
      my @cubes = split /\s*,\s*/;
      @cubes = map { [ m/(\d+)\s+(\w+)/ ] } @cubes;
      %colors = ( 
        %colors,
        map { 
          $_->[1] => ($colors{$_->[1]}, $_->[0])[$colors{$_->[1]} < $_->[0]] 
        } @cubes
      ); 
    }
    my $power = reduce { $a * $b } 1, values(%colors);
    $result += $power;
  }
  close(F);
  return $result;
}
