#!/usr/bin/perl

print("Part 1: " . &part1(@ARGV) . "\n");
print("Part 2: " . &part2(@ARGV) . "\n");

sub part1($) {
  open(F, $_[0]) or die "Couldn't open $_[0] for read: $!\n";
  my $schematic = [ ];
  while(<F>) {
    chomp;
    push(@$schematic, [ '.', split(''), '.' ]);
  }
  close(F);
  @empty = split('', '.' x @{$schematic->[0]});
  unshift(@$schematic, [ @empty ]);
  push(@$schematic, [ @empty ]);

  my $scan_perimeter = sub($$$) {
    my ($x, $y, $digits) = @_;
    my $len = length($digits);

    if ($schematic->[$x]->[$y] !~ /[0-9\.]/) {
      return 1;
    }
    if ($schematic->[$x]->[$y-$len-1] !~ /[0-9\.]/) {
      return 1;
    }
    for (my $i = $y-$len-1; $i <= $y; $i++) {
      if ($schematic->[$x-1]->[$i] !~ /[0-9\.]/) {
        return 1;
      }
      if ($schematic->[$x+1]->[$i] !~ /[0-9\.]/) {
        return 1;
      }
    }
    return 0;
  };

  my $result = 0;
  my $digits = '';
  for (my $x = 0; $x < @$schematic; $x++) {
    for (my $y = 0; $y < @{$schematic->[$x]}; $y++) {
      my $cell = $schematic->[$x]->[$y];
      if ($cell =~ /[0-9]/) {
        $digits .= $cell;
      } elsif ($digits ne '') {
        if (&$scan_perimeter($x, $y, $digits)) {
          $result += int($digits);
        }
        $digits = '';
      }
    }
  }

  return $result;
}

sub part2($) {
  open(F, $_[0]) or die "Couldn't open $_[0] for read: $!\n";
  my $schematic = [ ];
  while(<F>) {
    chomp;
    my $row = [ split('') ];
    while (/(\d+)/g) {
      my $pos = pos();
      my $val = $1;
      my $ref = \$val;
      for (my $i = $pos - length($val); $i < $pos; $i++) {
        $row->[$i] = $ref;
      }
    }
    $row = [ '.', @$row, '.' ];
    push(@$schematic, $row);
  }
  close(F);
  @empty = split('', '.' x @{$schematic->[0]});
  unshift(@$schematic, [ @empty ]);
  push(@$schematic, [ @empty ]);

  my $scan_perimeter = sub($$) {
    my ($x, $y) = @_;
    my $result = { };
    for (my $i = $x-1; $i <= $x+1; $i++) {
      for (my $j = $y-1; $j <= $y+1; $j++) {
        if (ref($schematic->[$i]->[$j])) {
          $result->{$schematic->[$i]->[$j]} = ${$schematic->[$i]->[$j]};
        }
      }
    }
    return $result;
  };

  my $result = 0;
  for (my $x = 0; $x < @$schematic; $x++) {
    for (my $y = 0; $y < @{$schematic->[$x]}; $y++) {
      if ($schematic->[$x]->[$y] eq '*') {
        my $ratios = &$scan_perimeter($x, $y);
        if (keys(%$ratios) == 2) {
          @vals = values(%$ratios);
          $result += $vals[0] * $vals[1];
        }
      } 
    }
  }

  return $result;
}
