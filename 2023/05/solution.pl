#!/usr/bin/perl

use List::Util qw/min/;

print("Part 1: " . &part1(@ARGV) . "\n");
#print("Part 2: " . &part2(@ARGV) . "\n");

sub part1($) {
  my ($resolver, $seeds) = &parse_almanac($_[0]);
  my @locations = map { &{$resolver}('seed', 'location', $_) } @$seeds;
  return min(@locations);
}

sub part2($) {
  my ($resolver, $seeds) = &parse_almanac($_[0]);
  my @planted = ();
  for (my $i = 0; $i < @$seeds; $i+=2) {
    my $low = $seeds->[$i];
    my $high = $start + $seeds->[$i+1];
    push (@planted, [ $low, $high ]);
  }
  return 0;
}

sub parse_almanac($) {
  open(F, $_[0]) or die "Couldn't open $_[0] for read: $!\n";
  my @seeds = ();
  my %maps = ();
  my $current = [ ];
  while(<F>) {
    chomp;
    if (/^seeds:\s+(.*)$/) {
      @seeds = $1 =~ /(\d+)/g;
    } elsif (/^(\w+)-to-(\w+) map:/) {
      $current = [ $2, sub ($) {
        return $_[0];
      } ];
      $maps{$1} = $current;
    } elsif (/^(\d+)\s+(\d+)\s+(\d+)$/) {
      my ($dest, $source, $range) = ($1, $2, $3);
      my $nested = $current->[1];
      my $resolver = sub ($) {
        if (($_[0] >= $source) && ($_[0] < $source + $range)) {
          return $_[0] + ($dest - $source);
        } else {
          return &$nested($_[0]);
        }
      };
      $current->[1] = $resolver;
    }
  }
  close(F);

  my $resolver = sub ($$$) {
    my ($source, $dest, $number) = @_;
    my $resolved = $number;
    for (my $key = $source; $key ne $dest; $key = $maps{$key}->[0]) {
      $resolved = &{$maps{$key}->[1]}($resolved);
    }
    return $resolved;
  };

  return ($resolver, \@seeds);
}
