#!/usr/bin/perl

%numbers = ( 
  "one"   => 1,
  "two"   => 2,
  "three" => 3,
  "four"  => 4,
  "five"  => 5,
  "six"   => 6,
  "seven" => 7,
  "eight" => 8,
  "nine"  => 9,
  "1" => 1,
  "2" => 2,
  "3" => 3,
  "4" => 4,
  "5" => 5,
  "6" => 6,
  "7" => 7,
  "8" => 8,
  "9" => 9
);

open(F, "$ARGV[0]") or die "Couldn't open $ARGV[0] for read: $!\n";
$result = 0;
while (<F>) {
  my $re = '(' . join('|', keys(%numbers)) . ')';
  my ($first) = m/$re/g;
  my ($last) = m/^.*$re.*?$/;
  $result += int("$numbers{$first}$numbers{$last}");
}
close(F);
print $result;
