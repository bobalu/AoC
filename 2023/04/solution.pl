#!/usr/bin/perl

use List::Util qw/sum/;

print("Part 1: " . &part1(@ARGV) . "\n");
print("Part 2: " . &part2(@ARGV) . "\n");

sub part1($) {
  my @matches = &parse_matches($_[0]);
  foreach (@matches) {
    $result += 2 ** ($_ - 1) if ($_);
  }
  return $result;
}

sub part2($) {
  my @matches = &parse_matches($_[0]);
  my %copies = ();
  for (my $i = 0; $i < @matches; $i++) {
    $copies{$i}++;
    for (my $j = 1; $j <= $matches[$i]; $j++) {
      $copies{$i + $j} += $copies{$i};
    }
  }
  return sum(values(%copies));
}

sub parse_matches($) {
  open(F, $_[0]) or die "Couldn't open $_[0] for read: $!\n";
  my @matches = ();
  while(<F>) {
    chomp;
    my @parsed = m/^Card\s+(\d+):\s+(.*?)\s+\|\s+(.*?)$/;
    my %winning = map { $_ => 1 } ( $parsed[1] =~ /(\d+)/g );
    my @drawn = $parsed[2] =~ /(\d+)/g;
    my @matched = grep { exists($winning{$_}) } @drawn;
    push @matches, scalar(@matched);
  }
  close(F);
  return @matches;
}